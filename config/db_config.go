package config

import (
	"database/sql"
	"fmt"
	_ "github.com/go-sql-driver/mysql"
	"github.com/spf13/viper"
	"svc-insani-go/utils"
	"time"
)

func DatabaseConnection() *sql.DB {
	viper.SetConfigName("mysql")
	viper.AddConfigPath("config")

	handleReadingConfigFile := viper.ReadInConfig()

	utils.GlobalErrorException(handleReadingConfigFile)

	var CoroHost = viper.GetString("MySQL.Host")
	var CoroUser = viper.GetString("MySQL.Username")
	var CoroPass = viper.GetString("MySQL.Password")
	var CoroDB = viper.GetString("MySQL.DatabaseName")

	var databaseConfig = fmt.Sprintf("%s:%s@tcp(%s)/%s",
		CoroUser, CoroPass, CoroHost, CoroDB)

	databaseConnectionConfiguration, errorDatabaseConfiguration := sql.Open("mysql", databaseConfig)

	utils.DatabaseErrorException(errorDatabaseConfiguration)

	databaseConnectionConfiguration.SetMaxOpenConns(200)
	databaseConnectionConfiguration.SetMaxIdleConns(200)
	databaseConnectionConfiguration.SetConnMaxLifetime(200 * time.Millisecond)

	return databaseConnectionConfiguration
}