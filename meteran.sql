-- MySQL dump 10.13  Distrib 5.7.26, for Linux (x86_64)
--
-- Host: 127.0.0.1    Database: meteran
-- ------------------------------------------------------
-- Server version	5.7.26-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `meteran`
--

DROP TABLE IF EXISTS `meteran`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `meteran` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `meter_reads_reply_message` json DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `meteran`
--

LOCK TABLES `meteran` WRITE;
/*!40000 ALTER TABLE `meteran` DISABLE KEYS */;
INSERT INTO `meteran` (`id`, `meter_reads_reply_message`) VALUES (1,'{\"reply\": {\"reply_code\": \"0\", \"correlation_id\": \"353\"}, \"header\": {\"noun\": \"MeterReads\", \"verb\": \"reply\", \"source\": \"SICONIA\", \"revision\": \"1\", \"date_time\": \"2017-12-23T00:31:01-08:00\"}, \"payload\": {\"meter_reading\": {\"meter\": {\"mrid\": \"SAG654647\", \"id_type\": \"METER_X_UDC_ASSET_ID\"}, \"interval_block\": [{\"i_reading\": [{\"flags\": \"\", \"value\": \"5\", \"end_time\": \"2017-12-22T18:00:00-08:00\", \"collection_time\": \"2017-12-23T00:00:00-08:00\", \"interval_length\": 0}], \"reading_type_id\": \"P15MIN_1-0:32.24.0.255\"}, {\"i_reading\": [{\"flags\": \"0\", \"value\": \"5\", \"end_time\": \"2017-12-22T18:00:00-08:00\", \"collection_time\": \"2017-12-23T00:00:00-08:00\", \"interval_length\": 3600}, {\"flags\": \"0\", \"value\": \"6\", \"end_time\": \"2017-12-22T19:00:00-08:00\", \"collection_time\": \"2017-12-23T00:00:00-08:00\", \"interval_length\": 3600}], \"reading_type_id\": \"P15MIN_1-0:52.24.0.255\"}, {\"i_reading\": [{\"flags\": \"0\", \"value\": \"3.365\", \"end_time\": \"2017-12-22T19:00:00-08:00\", \"collection_time\": \"2017-12-23T00:00:00-08:00\", \"interval_length\": 3600}], \"reading_type_id\": \"P15MIN_1-0:72.24.0.255\"}]}}}');
/*!40000 ALTER TABLE `meteran` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-07-17 10:14:01
