package repository


func CreateNewMeteranData() string {
	return "INSERT INTO meteran (meter_reads_reply_message) VALUES (?)"
}

func FindMeteranById() string  {
	return "SELECT * FROM meteran WHERE id=?"
}

func FindAllMeteranWithLimit() string {
	return "SELECT * FROM meteran ORDER BY id LIMIT ? OFFSET ?"
}

func FindAllMeteran() string {
	return "SELECT * FROM meteran ORDER BY id"
}