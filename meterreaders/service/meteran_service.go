package service

import (
	"log"
	"test-backend/config"
	"test-backend/meterreaders/models"
	"test-backend/meterreaders/repository"
)

func InsertNewMeteranData(dataMeteran string) {
	conn := config.DatabaseConnection()

	sqlQuery := repository.CreateNewMeteranData()
	sqlStmt, err := conn.Prepare(sqlQuery)

	if err != nil {
		log.Print(err.Error())
	}

	_, err2 := sqlStmt.Exec(dataMeteran)

	if err2 != nil {
		log.Print(err.Error())
	}

	defer conn.Close()
}

func GetMeteranById(id string) models.MeteranReplyMessage {
	conn := config.DatabaseConnection()
	meteranData := models.MeteranReplyMessage{}

	sqlQuery := repository.FindMeteranById()
	result, err := conn.Query(sqlQuery, id)
	if err != nil {
		log.Print(err.Error())
	}

	for result.Next() {
		meteran := models.MeteranReplyMessage{}
		err := result.Scan(&meteran.Id, &meteran.MeteranReadsReplyMessage)
		if err != nil {
			log.Print(err.Error())
		}
		meteranData.Id = meteran.Id
		meteranData.MeteranReadsReplyMessage = meteran.MeteranReadsReplyMessage
	}

	defer conn.Close()

	return meteranData
}

func GetAllMeteran() []models.MeteranReplyMessage {
	conn := config.DatabaseConnection()
	var meteranData []models.MeteranReplyMessage

	sqlQuery := repository.FindAllMeteran()

	result, err := conn.Query(sqlQuery)
	if err != nil {
		log.Print(err.Error())
	}

	for result.Next() {
		meteran := models.MeteranReplyMessage{}
		err := result.Scan(&meteran.Id, &meteran.MeteranReadsReplyMessage)
		if err != nil {
			log.Print(err.Error())
		}
		meteranData = append(meteranData, meteran)
	}

	defer conn.Close()

	return meteranData
}
