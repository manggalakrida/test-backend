package models

import (
	"encoding/json"
	"encoding/xml"
)

// Reading from DB to JSON
type MeteranReplyMessage struct {
	Id                       int             `json:"id"`
	MeteranReadsReplyMessage json.RawMessage `json:"meteran_reads_reply_message"`
}

// Reading from XML to JSON
type Meteran struct {
	XMLName xml.Name `xml:"MeterReadsReplyMessage" json:"-"`
	Header  *Header  `xml:"header" json:"header"`
	Payload *payload `xml:"payload" json:"payload"`
	Reply   *Reply   `xml:"reply" json:"reply"`
}

type Header struct {
	Verb     string `xml:"verb" json:"verb"`
	Noun     string `xml:"noun" json:"noun"`
	Revision string `xml:"revision" json:"revision"`
	DateTime string `xml:"dateTime" json:"date_time"`
	Source   string `xml:"source" json:"source"`
}

type payload struct {
	MeterReading *MeterReading `xml:"MeterReading" json:"meter_reading"`
}

type MeterReading struct {
	Meter         *Meter          `xml:"Meter" json:"meter" json:"meter"`
	IntervalBlock []IntervalBlock `xml:"IntervalBlock" json:"interval_block"`
}

type Meter struct {
	MRID   string `xml:"mRID" json:"mrid"`
	IdType string `xml:"idType" json:"id_type"`
}

type IntervalBlock struct {
	ReadingTypeId string     `xml:"readingTypeId" json:"reading_type_id"`
	IReading      []IReading `xml:"IReading" json:"i_reading"`
}

type IReading struct {
	EndTime        string `xml:"endTime" json:"end_time"`
	IntervalLength int    `xml:"intervalLength" json:"interval_length"`
	Value          string `xml:"value" json:"value"`
	Flags          string `xml:"flags" json:"flags"`
	CollectionTime string `xml:"collectionTime" json:"collection_time"`
}

type Reply struct {
	ReplyCode     string `xml:"replyCode" json:"reply_code"`
	CorrelationId string `xml:"correlationId" json:"correlation_id"`
}
