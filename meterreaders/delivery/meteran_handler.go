package delivery

import (
	"encoding/json"
	"encoding/xml"
	"fmt"
	"github.com/labstack/echo"
	"io/ioutil"
	"net/http"
	"os"
	"test-backend/meterreaders/models"
	"test-backend/meterreaders/service"
)

func InsertNewData(c echo.Context) error {
	xmlFile, err := os.Open("Data.xml")
	// if we os.Open returns an error then handle it
	if err != nil {
		fmt.Println(err)
		result := echo.Map{"message ": "Data kosong!"}
		return c.JSON(http.StatusOK, result)
	}

	fmt.Println("Successfully Opened users.xml")
	// defer the closing of our xmlFile so that we can parse it later on
	defer xmlFile.Close()

	// read our opened xmlFile as a byte array.
	byteValue, _ := ioutil.ReadAll(xmlFile)

	var meteran models.Meteran
	xml.Unmarshal(byteValue, &meteran)
	jsonData, _ := json.Marshal(meteran)
	service.InsertNewMeteranData(string(jsonData))
	result := echo.Map{"message ": "Berhasil menambah data"}
	return c.JSON(http.StatusCreated, result)
}

func GetAllDataMeteran(c echo.Context) error {
	meteranData := service.GetAllMeteran()
	if meteranData == nil {
		result := echo.Map{"message ": "Data kosong!"}
		return c.JSON(http.StatusOK, result)
	}
	result := echo.Map{
		"total": len(meteranData),
		"count": len(meteranData),
		"data":  meteranData,
	}
	return c.JSON(http.StatusOK, result)
}

func GetMeteranDataById(c echo.Context) error {
	id := c.Param("id")
	meteranData := service.GetMeteranById(id)
	if &meteranData == nil {
		result := echo.Map{"message ": " Data tidak ditemukan"}
		return c.JSON(http.StatusNotFound, result)
	}
	return c.JSON(http.StatusOK, meteranData)
}