package main

import (
	"github.com/labstack/echo"
	"github.com/labstack/echo/middleware"
	"github.com/labstack/gommon/log"
	"test-backend/meterreaders/delivery"
)

func main() {
	e := echo.New()

	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: []string{"*"},
		AllowMethods: []string{echo.GET, echo.POST, echo.PUT, echo.DELETE},
	}))

	e.POST("/meteran", delivery.InsertNewData)
	e.GET("/meteran", delivery.GetAllDataMeteran)
	e.GET("/meteran/:id", delivery.GetMeteranDataById)

	e.Use(middleware.Logger())
	e.Use(middleware.Recover())

	if e.Debug {
		e.Logger.SetLevel(log.DEBUG)
	}

	e.Logger.Fatal(e.Start(":8000"))
}
